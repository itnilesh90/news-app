var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var uglify = require('gulp-uglify'); 
var zip = require('gulp-zip');


var paths = {
  sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', ['sass'], function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

// For Compressing our js File and can find a minified all.min.js file in News Folder
gulp.task('compress', function () { 
    return gulp.src('./www/js/*.js') 
        .pipe(zip('archive.zip')) 
        .pipe(gulp.dest('News')); 
});

gulp.task('scripts',['compress'],function(){ //we can specify the sequence of task.(like bold task)
  gulp.src('./www/js/*.js') 
  .pipe(concat('all.min.js')) //this will concat all js file in /src folder and destination folder is dist folder and file is all.min.js
  .pipe(uglify()) // copy all js file from /src folder and paste in new destination folder name like dist
  .pipe(gulp.dest('News')) 
});
//End Code For Compressing our js File...


gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
