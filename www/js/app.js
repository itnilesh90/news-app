// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module APP (also set in a <body> attribute in index.html)

// 'starter.controllers' is found in controllers.js
// 'starter.service' is found in newsService.js


angular.module('starter', ['ionic', 'starter.controllers','starter.service'])

.run(function($ionicPlatform, $rootScope, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    //For Internet Connection 
    if (navigator.connection.type == Connection.NONE) { 
       $ionicPopup.confirm({
           title: "Internet Disconnected",
           content: "The internet is disconnected on your device."
         })
         .then(function(result) { 
           if (!result) { 
             ionic.Platform.exitApp();
           }
         });
    }
    //For Internet Connection 


  });

})

.config(function($stateProvider, $urlRouterProvider) { 
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.video', {
    url: '/video',
    views: {
      'menuContent': {
        templateUrl: 'templates/video.html'
      }
    }
  })

  .state('app.browse', { 
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
          controller: 'NewslistsCtrl'
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/browse');
})






