angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $rootScope, NewsFeed,  $ionicLoading, $ionicScrollDelegate) { 

  $scope.latestItem = "News";
  $scope.latestSubCategory = "Top News";
  $rootScope.top_news = true;
  var nullData = null;
  var dataForm = null;
  var method = 'GET';
  $scope.searchForce = {};
  $scope.searchForce.category = '';

 /*Start Redirect Full News of Times of India Page */
  $scope.go_to_item = function(display) {
      var b = '';

      if($scope.latestSubCategory == 'Photos' || $scope.latestSubCategory == 'Videos'){
        b = display.WebUrl;
      }
      else{
        b = display.WebURL;
      }
      window.open(b, '_blank','heigth=600,width=600');   // may alse try $window
  };
  /*End Redirect Full News of Times of India Page */

/*For Ionic loading show and hide function*/
  $scope.show = function() { 
    $ionicLoading.show({
      template: '<p>Loading...</p><ion-spinner></ion-spinner>'
    });
  };

  $scope.hide = function(){ 
        $ionicLoading.hide();
  };
/*end For Ionic loading show and hide function*/

/*Code Start To calling API for default Top News and Category Wise Subcattegory wise*/
  $rootScope.topNews = function(topCategoryData){ 
    $scope.searchForce.category = '';

    if(topCategoryData != null){
      var apiUrl = topCategoryData.sectionurl;/*'http://timesofindia.indiatimes.com/feeds/newsfeed/newsdefaultfeeds.cms?feedtype=sjson';*/
        $rootScope.latestData = apiUrl;
    }
    else{
      var apiUrl = 'http://timesofindia.indiatimes.com/feeds/newsfeed/newsdefaultfeeds.cms?feedtype=sjson';
      $rootScope.latestData = apiUrl;
    }

    method = 'GET';
    NewsFeed.async(apiUrl, method, dataForm).then(function(data) { 
      //$rootScope.totalPages = data.data.Pagination ?  data.data.Pagination.TotalPages : 1;

      if(topCategoryData == null){ 
        $rootScope.totalPages = 1;
        $rootScope.topNewsData = data.data.NewsItem;
        $ionicScrollDelegate.scrollTop();
      }
      else{ 
        apiUrl = data.data.Item[0].url;
        $rootScope.latestData = apiUrl;

        NewsFeed.async(apiUrl, method, dataForm).then(function(data) { 
          $rootScope.totalPages = data.data.Pagination != null ?  data.data.Pagination.TotalPages : 1;
          $rootScope.topNewsData = data.data.NewsItem;
          $ionicScrollDelegate.scrollTop();
        });

      }
      $scope.hide(); //for hiding loading
    });
  }
/*Code End To calling API for default Top News and Category Wise Subcattegory wise*/
  
  /*For first Time call with NULL data to*/
  $rootScope.topNews(nullData);

/*this section for menu.html category Subcategory wise API calling and to make data flow*/
  $scope.getCategoryNews = function(data){ 
    $scope.searchForce.category = '';

    $scope.show();
    $timeout( function(){
        $scope.hide();
    }, 10000 );

    $rootScope.top_news = false;
    $scope.latestSubCategory = data.name;
    if(data.name == 'Top News'){
      $rootScope.top_news = true;
      $rootScope.topNews(data);
    }
    else if(data.name == 'India'){
      $rootScope.top_news = true;
      $rootScope.topNews(data);
    }
    else if(data.name == 'Science'){
      $rootScope.top_news = true;
      $rootScope.topNews(data);
    }
    else{ 
      $rootScope.top_news = false;
      var apiUrl = data.sectionurl;
      method = 'GET';
      NewsFeed.async(apiUrl, method, dataForm).then(function(data) {
          $rootScope.totalPages = data.data.Pagination != null ?  data.data.Pagination.TotalPages : 1;

          $scope.latestCategory = data.data.Item;
          $scope.latestItem = data.data.Item.category;
          $ionicScrollDelegate.scrollTop();
          $scope.hide();
      });
    }

  }
/*end code this section for menu.html...*/

//Pull to refresh code start here 
  $scope.doRefresh = function(topNews) { 
    var apiUrl = $rootScope.latestData;
    method = 'GET';
    NewsFeed.async(apiUrl, method, dataForm).then(function(data) {
        $rootScope.topNewsData = data.data.NewsItem;
        $ionicScrollDelegate.scrollTop();
        $scope.$broadcast('scroll.refreshComplete');
    });
  };
//Pull to refresh code start here 
  

// Perform the find Category action when Load First time
  $scope.getnewsCat = function() { 
      var data = null;
      var apiUrl = 'https://devru-times-of-india.p.mashape.com/feeds/feedurllist.cms?catagory=city';
      method = 'POST';

      NewsFeed.async(apiUrl, method, data).then(function(data) {
          $scope.allNewsCategory = data.data.Item;
          $ionicScrollDelegate.scrollTop();
       });
  }
// Perform the find Category action when Load First time
})

.controller('NewslistsCtrl', function($scope, $rootScope, $window, $timeout, NewsFeed, $ionicLoading, $ionicScrollDelegate) { 
      var dataForm = null;
      var method = 'GET';

      $scope.show = function() { 
        $ionicLoading.show({
          template: '<p>Loading...</p><ion-spinner></ion-spinner>'
        });
      };

      $scope.page = 0;

      $scope.hide = function(){ 
            $ionicLoading.hide();
      };

//Start Code for Lazy Loading for more Data
      $scope.loadMore = function(){ 
        $scope.page =$scope.page+1;

        var apiUrl = $rootScope.latestData + '&page='+$scope.page;

        NewsFeed.async(apiUrl, method, dataForm).then(function(data) { 
          //if(data.data.Pagination.TotalPages > 1 ){ 
            for (var i=0; i< data.data.NewsItem.length; i++){
                $rootScope.topNewsData.push(data.data.NewsItem[i]);
            }
          //}
            $scope.hide();
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
      }
//End Code for Lazy Loading...

//to API call according to sub Category
      $scope.getNewsFeed =function(data){ 
        $scope.page = 1;

        $scope.show(); 
        $timeout( function(){
            $scope.hide();
        }, 10000 );

        $rootScope.top_news = true;

        $scope.ltCategoryHead = data.category; 
        var apiUrl = data.url;
        $rootScope.latestData = apiUrl;

        NewsFeed.async(apiUrl, method, dataForm).then(function(data) {

            $rootScope.totalPages = data.data.Pagination != null ?  data.data.Pagination.TotalPages : 1;

            $rootScope.topNewsData = data.data.NewsItem;
            $ionicScrollDelegate.scrollTop();
            $scope.$broadcast('scroll.refreshComplete');

            $scope.hide();
        });
      }
//End Codeto API call according to sub Category

}) // Controller End Here


//Directive for Alternative image when Images URL comes but wrong...
.directive("hires", function() { 
    return {
        link: function(scope, element, attrs) {
            var img, loadImage;
            img = null;
            loadImage = function() {
                img = new Image();
                img.src = attrs.hires;
                img.onload = function() {
                    element[0].src = attrs.hires;
                };
            };

            scope.$watch((function() {
                return attrs.hires;
            }), function(newVal, oldVal) {
                loadImage();
            });
        }
    };
});
//End Code Directive for Alternative image when Images URL comes but wrong...


