angular.module('starter.service', [])
.service('NewsFeed', function($http) {

    var http = {
        async: function(apiUrl, method, data) { 
            var promise;            
            var urlAddress = apiUrl;
            
            if (method == 'POST') {
                var config = {
                    headers : {
                        'X-Mashape-Key': 'ZwbmmT4UMtmsh2QdbyvauPfpuG1Zp14aUJ6jsnW1QQE5OBuBLs'
                    }
                }
                
                promise = $http.post(urlAddress, data, config)
                .success(function (data, status, headers, config) {
                    //$scope.PostDataResponse = data;
                    //console.log("Sucess :: ", data);
                    return data;
                })
                .error(function (data, status, header, config) {
                    console.log("ERROR :: ", data);
                    return data;
                });
            }
            else if (method == 'GET') {
                promise = $http.get(urlAddress, { cache: false }).then(function(responseData) {
                    //console.log("promise :: ", promise);
                    //console.log("response :: ", responseData);
                    //console.log("response.data :: ", response.data);
                    //return promise;
                    return responseData;
                },
                function(responseError){
                    // failure call back
                    console.log("response :: ", responseError);
                    return responseError;
                });
            }
            return promise;
        }
    };
    return http;
})

